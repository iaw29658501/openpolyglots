<?php

namespace App\Tests;

use App\Entity\Lesson;
use PHPUnit\Framework\TestCase;

class LessonTest extends TestCase
{
    public function testHelloWorld(){
        $lessonText="Hello world!";
        $expected=[
            [
                [
                    [ 'content'=>'Hello','clickable'=>true, 'index'=>1 ],
                    [ 'content'=>' ','clickable'=>false,'index'=>2],
                    [ 'content'=>'world','clickable'=>true,'index'=>3],
                    [ 'content'=>'!','clickable'=>false,'index'=>4]
                ]
            ]
        ];
        $lesson = new Lesson();
        $lesson->setPages($lessonText);
        self::assertSame($lesson->getPages(), $expected);
    }

//    public function testSimpleExample(): void
//    {
//        $given = 'A mis soledades voy.De mis soledades vengo';
//        $expected =["A mis soledades voy.","De mis soledades vengo"] ;
//        $lesson = new Lesson($given);
//        $lesson->setPages($given);
//        self::assertSame($lesson->getPages(), $expected);
//    }
//
//    public function testDoblePunctuationExample()
//    {
//        $given='Hercle, hydra emeritis!! Fluctui! Hercle, era fatalis ... germanus cacula!';
//        $expected=['Hercle,',' hydra emeritis!!',' Fluctui!', ' Hercle,',' era fatalis ...',' germanus cacula!'];
//        $lesson = new Lesson($given);
//        $lesson->setPages($given);
//        self::assertSame($lesson->getPages(), $expected);
//    }


    /*
     * "Hello world!"
     * 'pages':[
          [
            [{'content':'Hello','index':1},{'content':' ','clickable':false,'index':2},{'content':'world','index':3},{'content':'!','clickable':false,'index':4}],
            [{'content':'Show','index':5},{'content':' ','clickable':false,'index':6},{'content':'me','index':7},{'content':' ','clickable':false,'index':8},{'content':'a','index':9},{'content':' ','clickable':false,'index':10},{'content':'trick','isNew':true,'index':11},{'content':' ','clickable':false,'index':12},{'content':'please','index':13},{'content':'?','index':14}],
            [{'content':'Yes','index':15},{'content':' ','clickable':false,'index':16},{'content':'I','index':17},{'content':' ','clickable':false,'index':18},{'content':'can','index':19},{'content':'.','clickable':false,'index':20}],
            [{'content':'Think','index':21},{'content':' ','clickable':false,'index':22},{'content':'into','index':23},{'content':' ','clickable':false,'index':24},{'content':'something','index':25}]
          ],
          [
              [{'content':'This','index':26},{'content':' ','clickable':false,'index':27},{'content':'is','index':28},{'content':' ','clickable':false,'index':29},{'content':'an','index':30},{'content':' ','clickable':false,'index':31},{'content':'example','index':32}]
          ]
        ]
     */

//    public function testPagePhrasesAndWords()
//    {
//        $given="We are all humans, but what is this? Lets make some example text because remember, this is a test!";
//        $expected=[
//          [
//            [
//                "We"," ","are"," ","all"," ","humans"," ","but", "what", "is", "this?", "Lets", " ", "make", " ","some",
//                " ", "example", " ","text"," ", "because"," ", "remember",",", "this"," ","is"," ","a"," ","test","!"
//            ]
//          ]
//        ];
//    }

}
