<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LessonRepository;
use App\Service\PageTransformer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LessonRepository::class)
 * @ApiResource(
 *     itemOperations={"get"},
 *     collectionOperations={}
 * )
 */
class Lesson
{
    public const PHRASES_PER_PAGE=6;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private $title;

    /**
     * @ORM\Column(type="json")
     */
    private $pages = [];

    /**
     * @ORM\Column(type="string", length=63, nullable=true)
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPages(): ?array
    {
        return $this->pages;
    }

    public function setPages(string $lessonText): self
    {
        $phrases = preg_split("/([a-zA-z\s_\-0-9]*[,.?!][,.?!\-_]*)/", $lessonText, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $wordCount=0;
        foreach($phrases as $index => $phrase) {
            $words= preg_split('/([\s,.?!])+/',$phrase,-1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE) ;
            foreach($words as $index2 => $word) {
                $wordCount++;
                $wordWithProps=[
                    'content'=>$word,
                    'clickable'=>!preg_match('/[\s,.?!]/',$word[0]),
                    'index'=>$wordCount
                ];
                $words[$index2]=$wordWithProps;
            }
            $phrases[$index]=$words;
        }
        $pages = array_chunk($phrases,30);
        if ($pages) {
            $this->pages = $pages;
        }
        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
