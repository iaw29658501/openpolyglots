<?php

namespace App\DataFixtures;

use App\Entity\Lesson;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $lesson = new Lesson();
         $lesson->setTitle('7 Languages That Will Actually Make Your Life Better.');
         $lesson->setPages('We all know that languages are important to communicate and connect with people but do you know which ones are the best to do that in the large scale? 1. English , the most widely spread language on the world! 2. Spanish, present in almost all continents known, 3. Mandarin (Chinese) A font, defined as a set of type or characters all of one style, can "perpetuate problematic stereotypes," according to CNN. The network’s verified Twitter account wrote, "For years, the West has relied on so-called ‘chop suey’ fonts to communicate ‘Asianness’ in food packaging, posters and ad campaigns. But such fonts perpetuate problematic stereotypes," to accompany a report examining the theory."CNN has reached the epitome of \'we\'re out of stuff to pretend to be outraged by this week for clicks\' if they\'re surmising which fonts are racist," political satirist Tim Young told Fox News last week.
"This piece leaves me with more questions than virtue signals," Young continued. "Which fonts belong to which demographics? Does this mean Times New Roman is a Caucasian font? Is the old typewriter font for elderly people? Are there straight and gay fonts or fonts based on the 54 genders? I need to know. I\'m so confused at this point." ');
         $lesson->setAuthor('anon');
         $manager->persist($lesson);

        $manager->flush();
    }
}
