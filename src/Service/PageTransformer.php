<?php
namespace App\Service;

class PageTransformer
{
    public function content2array($lessonContent): array
    {
        $phrases = preg_split("/([a-zA-z\s_\-0-9]*[,.?!][,.?!\-_]*)/", $lessonContent, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $wordCount=0;
        foreach($phrases as $index => $phrase) {
            $words= preg_split('/([\s,.?!])+/',$phrase,-1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE) ;
            foreach($words as $index2 => $word) {
                $wordCount++;
                $wordWithProps=[
                    'content'=>$word,
                    'clickable'=>!preg_match('/[\s,.?!]/',$word[0]),
                    'index'=>$wordCount
                ];
                $words[$index2]=$wordWithProps;
            }
            $phrases[$index]=$words;
        }
        return array_chunk($phrases,30);
    }
    public function array2content($pages): string
    {
        $lessonContent ="";
        foreach($pages as $page) {
            foreach ($page as $phrase){
                foreach ($phrase as $word) {
                    $lessonContent .= $word['content'];
                }
            }
        }
        return $lessonContent;
    }
}