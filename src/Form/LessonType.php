<?php

namespace App\Form;

use App\Entity\Lesson;
use App\Service\PageTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class LessonType extends AbstractType
{
    /**
     * @var PageTransformer
     */
    private $pageTransformer;

    /**
     * LessonType constructor.
     * @param PageTransformer $pageTransformer
     */
    public function __construct(PageTransformer $pageTransformer)
    {

        $this->pageTransformer = $pageTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('pages',TextareaType::class);
            $builder->get('pages')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    // transform the array to a string
                    return $this->pageTransformer->array2content($tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return $tagsAsString;
                }
            ));
        $builder->add('author')
            ->add('image', FileType::class, [
                'label' => 'Lesson image',
                'mapped' => false,
                'required' => false,
                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PNG or JPG image',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lesson::class,
        ]);
    }
}
