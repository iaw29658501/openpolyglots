import Vue from 'vue';
import ComponentApp from './vue/app';

new Vue({
    render: (h) => h(ComponentApp)
}).$mount('#app');