import * as Driver from 'driver.js';
import 'driver.js/dist/driver.min.css';
const myDriver = new Driver();
setTimeout(function () {
    myDriver.defineSteps([
        {
            element:'.lecture-container',
            popover: {
                title: 'Welcome to OpenPoli!',
                description: 'Click any word to learn it',
            }
        },
        {
            element:'.dictionary .header',
            popover: {
                title: 'Learn',
                description: 'Select one Search Engine',
            }
        },
        {
            element:'.dictionary-options',
            popover: {
                title: 'Learn',
                description: 'Select one Search Engine',
            }
        }
    ]);
    myDriver.start();
}, 2000)

